FROM python:3.6

ENV PYTHONUNBUFFERED 1

#Log
RUN mkdir -p /var/log/vente_privee_backend/
RUN touch /var/log/vente_privee_backend/debug.log
RUN touch /var/log/vente_privee_backend/error.log
RUN touch /var/log/vente_privee_backend/access.log

RUN mkdir -p /opt/le2424/balenciaga/vente_privee_backend/

WORKDIR /opt/le2424/balenciaga/vente_privee_backend

ADD requirements.txt /opt/le2424/balenciaga/vente_privee_backend/

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN pip3 install gunicorn

ADD . /opt/le2424/balenciaga/vente_privee_backend/
