from django.contrib import admin
from .models import *


class MyUserAdmin(admin.ModelAdmin):
    list_array = ["pk", "email", "username", "first_name", "last_name", "date_joined"]
    list_display = list_array
    list_filter = ["last_name"]
    search_fields = ["email", "username", "last_name"]


admin.site.register(MyUser, MyUserAdmin)
