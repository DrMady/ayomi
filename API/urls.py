from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from django.urls import path
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from django.contrib import admin


from .views import *


schema_view = get_schema_view(
   openapi.Info(
      title="Test",
      default_version='1.1',
      description="This documents all end points for the API",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="No"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.register(r'users', MyUserViewSet)


urlpatterns = [
    # Usually you'd use Djoser for this but this will have to do for simplicity
    # Uses /login & /logout , doesnt work with postman unfortunately
    path(r'auth/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^user/me', CurrentMyUser.as_view()),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
