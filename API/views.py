from rest_framework import viewsets, generics
from rest_framework.permissions import IsAuthenticated

from .serializers import *


# Mmh yes this view is unprotected. Too bad!
# This can be changed by adding the authentication permission or going back to a generic for creation purposes
# Though this wont be done for simplicity

class MyUserViewSet(viewsets.ModelViewSet):
    queryset = MyUser.objects.all()
    serializer_class = MyUserSerializer
    # permission_classes = (IsAuthenticated,)


class CurrentMyUser(generics.RetrieveUpdateAPIView):
    serializer_class = MyUserDetailSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user
