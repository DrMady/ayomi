from rest_framework import serializers

from .models import *


class MyUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyUser
        fields = "__all__"

    # Since we're not with Djoser this is necessary to create a user instance
    def create(self, validated_data):
        password = validated_data.pop('password')
        user = MyUser(**validated_data)
        user.set_password(password)
        user.save()
        return user


class MyUserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyUser
        exclude = ["password", "last_login", "is_superuser", "user_permissions", "is_staff"]

