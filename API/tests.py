from rest_framework.test import APITestCase
import json

from .models import *

# Create your tests here.


class AppointmentTest(APITestCase):
    def setUp(self):
        MyUser.objects.create_superuser(username="root", password="root", email="root@root.com")
        login_data = {
            "username": "root",
            "password": "root"
        }
        response = self.client.post("http://localhost:8000/api/auth/token/login/", login_data)
        self.assertEqual(200, response.status_code)
        token = json.loads(response.content)['auth_token']
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token)

    def test_create_appointment(self):
        user_data = {
            "appointment_place": "test"
        }
        response = self.client.post('http://127.0.0.1:8000/appointments/', user_data, format='json', )
        self.assertEqual(201, response.status_code)

    def test_create_appointment_no_permission(self):
        self.client.credentials()
        user_data = {
            "appointment_place": "test"
        }
        response = self.client.post('http://127.0.0.1:8000/appointments/', user_data, format='json')
        self.assertEqual(401, response.status_code)

    def test_get_appointment(self):
        self.test_create_appointment()
        response = self.client.get('http://127.0.0.1:8000/appointments/1/', follow=True)
        self.assertEqual(200, response.status_code)

    def test_get_appointments(self):
        self.test_create_appointment()
        response = self.client.get('http://127.0.0.1:8000/appointments/', follow=True)
        self.assertEqual(200, response.status_code)

    def test_update_appointment(self):
        self.test_create_appointment()
        user_data = {
            "appointment_place": "newTest"
        }
        response = self.client.patch('http://127.0.0.1:8000/appointments/1/', user_data, format='json')
        self.assertEqual(200, response.status_code)

    def test_update_appointment_no_permission(self):
        self.test_create_appointment()
        self.client.credentials()
        user_data = {
            "appointment_place": "newTest"
        }
        response = self.client.patch('http://127.0.0.1:8000/appointments/1/', user_data, format='json')
        self.assertEqual(401, response.status_code)

    def test_delete_appointment(self):
        self.test_create_appointment()
        response = self.client.delete('http://127.0.0.1:8000/appointments/1/')
        self.assertEqual(204, response.status_code)

    def test_delete_appointment_no_permission(self):
        self.test_create_appointment()
        self.client.credentials()
        response = self.client.delete('http://127.0.0.1:8000/appointments/1/')
        self.assertEqual(401, response.status_code)


class UserTest(APITestCase):
    def setUp(self):
        MyUser.objects.create_superuser(username="root", password="root", email="root@root.com")
        login_data = {
            "username": "root",
            "password": "root"
        }
        response = self.client.post("http://localhost:8000/api/auth/token/login/", login_data)
        self.assertEqual(200, response.status_code)
        token = json.loads(response.content)['auth_token']
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token)

    def test_create_user(self):
        user_data = {
            "username": "test"
        }
        response = self.client.post('http://127.0.0.1:8000/users/', user_data, format='json', )
        self.assertEqual(201, response.status_code)

    def test_create_user_no_permission(self):
        self.client.credentials()
        user_data = {
            "username": "test"
        }
        response = self.client.post('http://127.0.0.1:8000/users/', user_data, format='json')
        self.assertEqual(401, response.status_code)

    def test_get_user(self):
        self.test_create_user()
        response = self.client.get('http://127.0.0.1:8000/users/2/', follow=True)
        self.assertEqual(200, response.status_code)

    def test_get_users(self):
        self.test_create_user()
        response = self.client.get('http://127.0.0.1:8000/users/', follow=True)
        self.assertEqual(200, response.status_code)

    def test_update_user(self):
        self.test_create_user()
        user_data = {
            "username": "newTest"
        }
        response = self.client.patch('http://127.0.0.1:8000/users/2/', user_data, format='json')
        self.assertEqual(200, response.status_code)

    def test_update_user_no_permission(self):
        self.test_create_user()
        self.client.credentials()
        user_data = {
            "username": "newTest"
        }
        response = self.client.patch('http://127.0.0.1:8000/users/2/', user_data, format='json')
        self.assertEqual(401, response.status_code)

    def test_delete_appointment(self):
        self.test_create_user()
        response = self.client.delete('http://127.0.0.1:8000/users/2/')
        self.assertEqual(204, response.status_code)

    def test_delete_user_no_permission(self):
        self.test_create_user()
        self.client.credentials()
        response = self.client.delete('http://127.0.0.1:8000/users/2/')
        self.assertEqual(401, response.status_code)


