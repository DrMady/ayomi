import React, { useState } from "react"
import './App.css';
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import Home from './components/Home';
import PrivateRoute from './PrivateRoute';
import {AuthContext} from "./context/auth";
import Login from "./pages/Login";
import Signup from './pages/SignUp';

function App() {
    const existingTokens = JSON.parse(localStorage.getItem("tokens"));
    const [authTokens, setAuthTokens] = useState(existingTokens);

    const setTokens = (data) => {
        localStorage.setItem("tokens", JSON.stringify(data));
        setAuthTokens(data);
    };

    return (
        <AuthContext.Provider value={{authTokens, setAuthTokens: setTokens}}>
            <Router>
                <div>
                    <Route exact path="/" component={Home}/>
                    <Route path="/login" component={Login}/>
                    {/*<Route path="/signup" component={SignUp}/>*/}
                    {/*<PrivateRoute path="/admin" component={Admin}/>*/}
                </div>
            </Router>
        </AuthContext.Provider>
    );
}

export default App;
