import React, {Component} from 'react';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://127.0.0.1:8000/users/1/")
            .then(res => res.json())
            .then(
                (result) => {
                    // console.log(result)
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading</div>;
        } else {
            return (
                <div>
                    <ul>
                        {Object.keys(items).map(item => !console.log(items[item]) && (
                            <li key={items.item}>
                                {item}: {items[item]}
                            </li>
                        ))}
                    </ul>
                </div>
            );
        }
    }
}

export default Home